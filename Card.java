public class Card {
    private String suit;
    private String value;

    public Card (String suit, String value) {
        this.suit = suit;
        this.value = value;
    }

    public String getSuit() {
        return suit;
    }

    public String getValue() {
        return value;
    }

    public String toString() {
        return value + " of " + suit;
    }
	
	//Lab 7A part
	public double calculateScore() {
		double score = 0;
		
		if (value.equals("Jack")) {
			score += 11.0;
		} 
		else if (value.equals("Queen")) {
			score += 12.0;
		} 
		else if (value.equals("King")) {
			score += 13.0;
		} 
		else if (value.equals("Ace")) {
			score += 14.0;
		} 
		else {
			int numericValue = Integer.parseInt(value);
			score += numericValue;
		}

		
		if (suit.equals("Hearts")) {
			score +=0.4;
		}
		else if (suit.equals("Spades")) {
			score +=0.3;
		}
		else if (suit.equals("Diamonds")) {
			score +=0.2;
		}
		else if (suit.equals("Clubs")) {
			score +=0.1;
		}
		
		return score;
	}
}